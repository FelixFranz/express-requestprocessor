interface LoggerInterface {
	log: (level: LoggerLevelsEnum, message: string, source: string, details: any) => void;
	levels: {
		ERROR: LoggerLevelsEnum
		WARNING: LoggerLevelsEnum
		INFO: LoggerLevelsEnum
		DEBUG: LoggerLevelsEnum
		TRACE: LoggerLevelsEnum
	};
}

// @ts-ignore
enum LoggerLevelsEnum {
	ERROR = 0,
	WARNING = 1,
	INFO = 2,
	DEBUG = 3,
	TRACE = 4,
}

interface RequestProcessorInterface {
	Logger: LoggerInterface,
	configure: (conf: SystemConfigType) => RequestProcessor,
	start: () => RequestProcessor,
	stop: () => RequestProcessor,
	restart: () => RequestProcessor,
	process: (conf: ProcessConfigType) => void
}

type SystemConfigType = {
	log: {
		level: LoggerLevelsEnum
		output: (out: SystemConfigLogOutputType, message: string) => void,
	},
	serviceFolder: string,
	apiBasePath?: string,
	port: number,
	authorize?: (req: ReqType) => any,
	requestSize?: string,
	notFoundPage?: (req: ReqType, res: ResType) => any,
	docs?: boolean | {
		route?: string,
		title?: string,
		description?: string,
		version?: string,
		host?: string,
		basePath?: string,
		additionalDocsPath?: string[],
		schemes?: string[],
		produces?: string[],
		securityDefinitions?: any
	}
}

export type SystemConfigLogOutputType = {
	level: LoggerLevelsEnum,
	message: string,
	source: string,
	details: any
}

type ProcessConfigType = {
	path: string,
	role?: string,
	get?: ProcessConfigMethodType
	post?: ProcessConfigMethodType
	put?: ProcessConfigMethodType
	delete?: ProcessConfigMethodType
	validate?: (req: any) => any,
	link?: Array<{ name: string, url: string }>
}

type ProcessConfigMethodType = (req: ReqType, res: ResType) => any | {
	handle: (req: ReqType, res: ResType) => any,
	validate: (req: ReqType) => any,
	role: string,
	link: Array<{ name: string, url: string }>
} | undefined

export type ReqType = {
	method: RequestMethods
	query: any
	path: any
	headers: any
	body: any
	role: any
	auth?: any
	endpoint: string
}

export type ResType = {
	status: number,
	body: any,
	header: object,
	cookie: object,
	file: string
}

enum RequestMethods {
	GET = "GET",
	POST = "POST",
	PUT = "PUT",
	DELETE = "DELETE"
}

// @ts-ignore
import * as RequestProcessor from "./RequestProcessor";

export default RequestProcessor;
