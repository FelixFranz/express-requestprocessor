const Path = require('path');
const fs = require('fs');

const Logger = require(`${__dirname}/Logger`);
let app;
let server;
let Config = null;
const cachedServiceConfigs = [];

module.exports = {
  configure: configure,
  start: start,
  stop: stop,
  restart: restart,
  process: processServices,
  Logger,
};

/**
 * Configures the request processor.
 * Must be called before calling `start`
 * @param {Object} conf Object that contains Logger config, port, service path
 * @return {Object} RequestProcessor
 */
function configure(conf) {
  if (!conf || !conf.log || !conf.log.level ||
        !conf.log.output || !conf.serviceFolder || !conf.port) {
    throw new Error('Wrong service configuration');
  }
  if (conf.apiBasePath === undefined) conf.apiBasePath = '';

  Config = conf;
  Logger.configure(conf.log.level, conf.log.output);
  Logger.log(Logger.levels.DEBUG, 'Activated Logger', 'RequestProcessor');
  return module.exports;
}

/**
 * Function to initialize and start the api endpoint server.
 * @return {Object} RequestProcessor
 */
function start() {
  if (!Config) {
    throw Error('You need to run `RequestProcessor.config(...)` first!');
  }
  app = require('express')();
  const bodyParser = require('body-parser');
  app.use(bodyParser.json({limit: Config.requestSize}));

  addEndpoints();
  if (Config.docs) addDocs();
  if (Config.notFoundPage) addNotFoundPage();

  server = app.listen(Config.port,
      () => Logger.log(Logger.levels.INFO, `Started listening on http://127.0.0.1:${Config.port}${Config.apiBasePath}`, 'RequestProcessor'));
  return module.exports;
}

/**
 * adds not found page from config if exists.
 */
function addNotFoundPage() {
  let path;
  let role;
  let handler;
  let validate;
  let link;
  if (typeof Config.notFoundPage === 'function') {
    handler = Config.notFoundPage;
  } else {
    path = Config.notFoundPage.path;
    role = Config.notFoundPage.role;
    handler = Config.notFoundPage.handler;
    validate = Config.notFoundPage.validate;
    link = Config.notFoundPage.link;
  }
  const process = (req, res) => {
    handleRequest(req, res, path || 'notFoundPage', role,
        (req, res) => {
          res.status = 404;
          return handler(req, res);
        }, validate, link);
  };
  app.use(process);
}

/**
 * Adds swagger docs using config, endpoint config and
 * npm module express-swagger-generator
 */
function addDocs() {
  const expressSwagger = require('express-swagger-generator')(app);

  const options = {
    route: {
      url: Config.apiBasePath + (Config.docs.route || '/docs'),
      docs: Config.apiBasePath + (`${Config.docs.route || '/docs'}.json`),
    },
    swaggerDefinition: {
      info: {
        title: Config.docs.title,
        description: Config.docs.description,
        version: Config.docs.version,
      },
      host: Config.docs.host,
      basePath: Config.docs.basePath || Config.apiBasePath || undefined,
      produces: Config.docs.produces || ['application/json'],
      schemes: Config.docs.schemes || ['http', 'https'],
      securityDefinitions: Config.docs.securityDefinitions,
    },
    basedir: Path.resolve('.'),
    files: [`${Config.serviceFolder}/**/*.js`,
      `${Config.serviceFolder}/**/*.ts`]
        .concat(Config.docs.additionalDocsPath || []),
  };
  expressSwagger(options);
  Logger.log(Logger.levels.DEBUG, 'Added api docs', 'RequestProcessor');
}

/**
 * Adds Endpoints by calling all files (services) in the servicePath folder.
 * These services should call `RequestProcessor.process`
 * to add themself to the api.
 */
function addEndpoints() {
  processCache();
  const paths = findServices(`${Path.resolve('.')}/${Config.serviceFolder}`);
  paths.forEach((p) => require(p));

  Logger.log(Logger.levels.INFO, 'Added endpoints', 'RequestProcessor');
}

/**
 * Recursive helper function to get all files of the servicePath folder.
 * @param {String} path of the service folder
 * @return {*[]}
 */
function findServices(path) {
  const result = fs.readdirSync(path);
  for (let i = 0; i < result.length; ++i) {
    result[i] = path + '/' + result[i];
    if (fs.lstatSync(result[i]).isDirectory()) {
      result[i] = findServices(result[i]);
    }
  }
  return [].concat([], ...result);
}

/**
 * Function to stop the api endpoint server.
 * @return {Object} RequestProcessor
 */
function stop() {
  server.close(() => Logger.log(Logger.levels.INFO,
      'Stopped server', 'RequestProcessor'));
  server = undefined;
  return module.exports;
}

/**
 * Function to restart the api endpoint server.
 * @return {Object} RequestProcessor
 */
function restart() {
  stop();
  start();
  return module.exports;
}

/**
 * This function should be called by a service.
 * It adds the configuration to the service cache and calles processConfig
 * for adding the service to the api.
 * @param {Object} config file that defines request methods,
 *  handler functions, validation functions and roles
 */
function processServices(config) {
  cachedServiceConfigs.push(config);
  processConfig(config);
};

/**
 * Processes all cached configs
 */
function processCache() {
  cachedServiceConfigs.forEach((config) => processConfig(config));
}

/**
 * Adds a service to the api using it's configuration.
 * @param {Object} config file that defines request methods,
 *  handler functions, validation functions and roles
 */
function processConfig(config) {
  const methods = {
    'GET': config.get,
    'POST': config.post,
    'PUT': config.put,
    'DELETE': config.delete,
  };

  config.path = Config.apiBasePath + config.path;

  for (const method in methods) {
    if (methods[method]) {
      let handle;
      let validate;
      let link;
      let role;

      if (typeof methods[method] == 'function') {
        handle = methods[method];
        validate = config.validate;
        role = config.role || [];
        link = config.link;
      } else if (methods[method].handle) {
        handle = methods[method].handle;
        validate = methods[method].validate || config.validate;
        role = methods[method].role || config.role || [];
        link = methods[method].link || config.link;
      }

      if (link !== undefined && !Array.isArray(link)) link = [link];

      const process = (req, res) =>
        handleRequest(req, res, config.path, role, handle, validate, link);

      switch (method) {
        case 'GET':
          app.get(config.path, process);
          break;
        case 'POST':
          app.post(config.path, process);
          break;
        case 'PUT':
          app.put(config.path, process);
          break;
        case 'DELETE':
          app.delete(config.path, process);
          break;
      }
    }
  }

  app.all(config.path, (req, res) => {
    res.header('Content-Type', 'application/json\'');
    res.status(405);
    res.send(JSON.stringify({error: 'Wrong request Method!'}));
  });
}

/**
 * Every api call will be redirected to this method
 * This method validates and handles the calls by calling service functions.
 *
 * @param {Object} expressReq express request object
 * @param {Object} expressRes express response object
 * @param {String} endpoint endpoint path
 * @param {String} role role for Authorizer
 * @param {Function} handle handle function (service function)
 * @param {Function} validate validate function (service function)
 * @param {Object} link for hypermedia
 */
async function handleRequest(expressReq, expressRes, endpoint,
    role, handle, validate, link) {
  expressRes.header('Content-Type', 'application/json\'');

  const req = {
    method: expressReq.method,
    query: expressReq.query,
    path: expressReq.params,
    headers: expressReq.headers,
    body: expressReq.body,
    auth: undefined,
    role,
    endpoint: endpoint,
  };

  if (Config.authorize) {
    try {
      req.auth = await Config.authorize(req);
    } catch (e) {
      expressRes.status(e.status || 403);
      expressRes.send(JSON.stringify({error: e.message}));
      Logger.log(Logger.levels.WARNING,
          `${e.message || 'Authorization failed'} `
            + `(${expressReq.method} ${endpoint})!`,
          'Authorizer', e);
      return;
    }
  }

  try {
    if (validate && false == await validate(req)) {
      expressRes.status(400);
      expressRes.send({error:
        `Validation failed for ${expressReq.method} ${endpoint}!`,
      });
      Logger.log(Logger.levels.DEBUG, 'Validation failed', 'RequestProcessor');
      return;
    }
  } catch (e) {
    expressRes.status(e.status || 400);
    expressRes.send({error: 'Validation failed!', details: e.message || e});
    Logger.log(Logger.levels.DEBUG,
        `Validation failed for ${expressReq.method} ${endpoint}!`
        ,
        'RequestProcessor', e);
    return;
  }

  const ResObj = function() {
    this.body = undefined;
    this.status = undefined;
    this.header = undefined;
    this.cookie = undefined;
    this.file = undefined;
  };
  const res = new ResObj();

  try {
    res.body = await handle(req, res) || res.body;
    res.status = res.status || (!res.body && !res.file ? 204 : undefined) ||
                ('POST' == req.method ? 201 : 200);
    expressRes.status(res.status);

    if (link !== undefined) {
      link = link.map((l) => {
        const pathParamRegex = /^.*\/(:[^\/]*).*$/;
        let pathParamPlaceHolder = pathParamRegex.exec(l.url);
        while (pathParamPlaceHolder !== null) {
          pathParamPlaceHolder = pathParamPlaceHolder[1];
          l.url = l.url.replace(pathParamPlaceHolder,
              req.path[pathParamPlaceHolder.slice(1)]);
          pathParamPlaceHolder = pathParamRegex.exec(l.url);
        }
        const responseRegex = /^.*\/(\$[^\/]*).*$/;
        let responsePlaceHolder = responseRegex.exec(l.url);
        while (responsePlaceHolder !== null) {
          responsePlaceHolder = responsePlaceHolder[1];
          l.url = l.url.replace(responsePlaceHolder,
              res.body[responsePlaceHolder.slice(8)]);
          responsePlaceHolder = responseRegex.exec(l.url);
        }
        return l;
      });
      link = link.map((l) => '<' + l.url +
                    '>; title="' + l.name + '"');
      expressRes.set('Link', link);
    }

    if (typeof res.header === 'object') {
      Object.keys(res.header).forEach((key) =>
        expressRes.set(key, res.header[key]));
    }
    if (typeof res.cookie === 'object') {
      Object.keys(res.cookie).forEach((key) =>
        expressRes.cookie(key, res.cookie[key].value, {
          maxAge: res.cookie[key].maxAge,
          domain: res.cookie[key].domain,
        })
      );
    }
    if (res.file) {
      expressRes.removeHeader('content-type');
      expressRes.sendFile(Path.resolve(res.file));
    } else {
      expressRes.send(expressRes.getHeader('content-type')
          .includes('application/json') ? JSON.stringify(res.body) : res.body);
    }
    Logger.log(Logger.levels.DEBUG, 'Handled ' + req.method + ' request for ' +
        expressReq.method + ' ' + endpoint + '!', 'RequestProcessor');
  } catch (e) {
    if (e.status && 100 <= e.status < 500) {
      Logger.log(Logger.levels.DEBUG,
          'Data could not be processed because an external reason for ' +
                expressReq.method + ' ' + endpoint + '!', 'RequestProcessor', {
            status: e.status,
            message: e.message,
            stack: e.stack,
          });
      expressRes.status(e.status);
      expressRes.send(JSON.stringify({
        error: 'Data could not be processed because an external reason!',
        details: e.message || e,
      }));
    } else {
      Logger.log(Logger.levels.WARNING, 'Could not handle request for ' +
          expressReq.method + ' ' + endpoint + '!', 'RequestProcessor', {
        status: e.status,
        message: e.message,
        stack: e.stack,
      });
      expressRes.status(e.status || 500);
      expressRes.send(JSON.stringify({
        error: 'Could not handle request!',
        details: e.message || e,
      }));
    }
  }
}
