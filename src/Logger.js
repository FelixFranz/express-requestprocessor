/**
 * all possible logging levels
 * @type {{ERROR, WARNING, INFO, DEBUG, TRACE}}
 */
const levels = {
  ERROR: 0,
  WARNING: 1,
  INFO: 2,
  DEBUG: 3,
  TRACE: 4,
};

let Config;

module.exports = {
  configure: configure,
  log: log,
  levels: levels,
};

/**
 * cofigures an output function that receives the logs
 * @param {level} level logging level
 * @param {Function} output output callback
 */
function configure(level, output) {
  Config = {level, output};
}

/**
 * Creates a log entry
 * @param {level} level Logging level provided by Logger.level
 * @param {String} message Logging message
 * @param {String} source Source of log entry
 * @param {Object} details (optional) Additional details
 */
function log(level, message, source, details) {
  if (!Config) {
    throw new Error('Logger not ready, use configure before logging');
  }
  if (!Config.level || level <= Config.level) {
    const out = {level, message, source, details};
    const parsedMessage = parseMessage(level, message, source, details);
    Config.output(out, parsedMessage);
  }
}

/**
 * Helper function to parse the logging message
 * @param {level} level Logging level provided by Logger.level
 * @param {String} message Logging message
 * @param {String} source Source of log entry
 * @param {Object} details (optional) Additional details
 * @return {String} parsed logging message
 */
function parseMessage(level, message, source, details) {
  if (details instanceof Error) {
    details = {
      message: details.message,
      stack: details.stack,
    };
  }
  return ''
    + new Date().toLocaleString()
    + ' [' + Object.keys(levels)[level] + ']: '
    + (source ? source + ': ' : '')
    + message
    + (details ? '\n' + JSON.stringify(details) : '');
}
