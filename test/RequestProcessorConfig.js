const RequestProcessor = require(`${__dirname}/../src/index`);

const securedRoles = ['services.user'];
const allowedUsers = [
  {
    name: 'user',
    password: 'pass',
  },
  {
    name: 'test',
    password: 'test',
  },
];

const authorizeBasicAuth = function(req) {
  const trowable = {status: 401, message: 'Authorization failed'};
  if (!req.headers.authorization) throw trowable;
  let authHeader = req.headers.authorization;
  if (!authHeader.startsWith('Basic ')) throw trowable;
  authHeader = authHeader.replace('Basic ', '');
  const login = Buffer.from(authHeader, 'base64').toString().split(':');

  for (let i = 0; i < allowedUsers.length; ++i) {
    if (allowedUsers[i].name === login[0]
            && allowedUsers[i].password === login[1]) return {user: login[0]};
  }
  throw trowable;
};

const authorize = function(req) {
  if (securedRoles.includes(req.role)) {
    return authorizeBasicAuth(req);
  }
};

module.exports = {
  log: {
    level: RequestProcessor.Logger.levels.DEBUG,
    output: (out, message) => console.log(message),
  },
  serviceFolder: `test/services`,
  apiBasePath: '',
  port: 8080,
  authorize,
  requestSize: '100kb',
  notFoundPage: (req, res) => {
    return 'Sorry this page was not found!';
  },
  docs: {
    route: '/docs',
    title: 'Request Processor Demo Services',
    description: 'For demonstration purpose!',
    version: '1.0.0',
    host: 'localhost:8080',
    basePath: '',
    schemes: ['http', 'https'],
    produces: ['application/json'],
    securityDefinitions: {
      basic: {
        type: 'basic',
      },
    },
  },
};
