const RequestProcessor = require(`${__dirname}/../../src/RequestProcessor`);

/**
 * Sums up two values provided by query parameters
 * @group Sum
 * @route GET /sum
 * @param {Integer} a.query Variable a
 * @param {Integer} b.query Variable b
 * @param {Integer} c.query Variable c
 * @param {Integer} n.query You can use an unlimited number of query parameters
 * @return {number} 200 - Result of the sum
 */

/**
 * Validates query parameters
 * @param {Object} req Request Object
 */
function validateGet(req) {
  Object.keys(req.query).forEach((q) => {
    if (Number.isNaN(parseFloat(req.query[q]))) {
      throw new Error('Only numbers are allowed as parameter');
    }
  });
}

/**
 * Sums up two values provided by query parameters
 * @example GET http://127.0.0.1:8080/sum?a=2&b=4
 * @param {Object} req
 * @param {Object} res
 * @return {string}
 */
function handleGet(req, res) {
  let result = 0;
  if (Object.keys(req.query).length !== 0) {
    Object.keys(req.query).forEach((q) => result += parseFloat(req.query[q]));
  }
  return result;
};

RequestProcessor.process({
  path: '/sum',
  role: 'services.sum',
  get: {
    validate: validateGet,
    handle: handleGet,
  },
  link: {
    name: 'Dispatcher',
    url: '/',
  },
});
