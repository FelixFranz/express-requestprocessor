const RequestProcessor = require(`${__dirname}/../../src/RequestProcessor`);

/**
 * Dispatcher Service returns information on the api root endpoint
 * @group General
 * @route GET /
 * @return {string} 200 - General information about this project with links
 */

/**
 * Dispatcher Service returns information on the api root endpoint
 * @param {Object} req
 * @param {Object} res
 * @return {string}
 */
function handleGet(req, res) {
  res.header= {
    'content-type': 'text/html',
  };
  return '<html><head><title>Request Processor</title></head>' +
    '<body><h1>Request Processor</h1><p>Request Processor demo Services!<br />Take a look at the <a href="/docs">generated documentation</a> or visit the <a href="https://www.npmjs.com/package/express-requestprocessor">npm registry</a> or <a href="https://gitlab.com/FelixFranz/express-requestprocessor/">repository</a> for more information!</p></body></html>';
};

RequestProcessor.process({
  path: '/',
  role: 'services.public',
  get: handleGet,
  link: [
    {
      name: 'Statuscode 200',
      url: '/statuscode/200',
    },
    {
      name: 'Statuscode 400',
      url: '/statuscode/400',
    },
    {
      name: 'Statuscode 500',
      url: '/statuscode/500',
    },
    {
      name: 'Statuscode 600',
      url: '/statuscode/600',
    },
    {
      name: 'Sum',
      url: '/sum',
    },
    {
      name: 'Sum',
      url: '/sum/<int>/<int>',
    },
    {
      name: 'User',
      url: '/user',
    },
  ],
});
