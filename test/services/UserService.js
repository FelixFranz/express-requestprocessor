const RequestProcessor = require(`${__dirname}/../../src/RequestProcessor`);

/**
 * Sums up two values provided by query parameters
 * @group User
 * @route GET /user
 * @security basic
 * @return {number} 200 - Result of the sum
 * @return {number} 401 - No or wrong Basic Auth Credentials provided.
 * Click Authorize and provide your Credentials!
 * Demo Users:
 *  * name: `user`, password: `pass`
 *  * name: `test`, password: `test`
 */

/**
 * Returns user name provided by basic auth
 * @example GET http://127.0.0.1:8080/user
 * @param {Object} req
 * @param {Object} res
 * @return {String}
 */
function handleGet(req, res) {
  return req.auth.user;
};

RequestProcessor.process({
  path: '/user',
  role: 'services.user',
  get: handleGet,
  link: {
    name: 'Dispatcher',
    url: '/',
  },
});
