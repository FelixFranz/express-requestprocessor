const RequestProcessor = require(`${__dirname}/../../src/RequestProcessor`);
const statusCodes = [200, 400, 500, 600];

/**
 * Service that returns provided status code
 * @group Status Code
 * @route GET /statuscode/{statuscode}
 * @route POST /statuscode/{statuscode}
 * @route PUT /statuscode/{statuscode}
 * @route DELETE /statuscode/{statuscode}
 * @param {Integer} statuscode.path.required Status Code
 * @return {string} 200 - Status 200 Test
 * @return {string} 400 - Status 400 Test
 * @return {string} 500 - Status 500 Test
 * @return {string} 600 - Status 600 Test
 */

/**
 * Builds a service that returns provided status code
 * @param {Number} code status code number
 * @param {Object} req Request Object
 * @param {Object} res Response Object
 * @return {undefined} Nothing
 */
function handleEverything(code, req, res) {
  res.status = code;
  return `Status ${code} Test`;
};

statusCodes.forEach((code ) => {
  RequestProcessor.process({
    path: `/statuscode/${code}`,
    role: `services.statuscode.${code}`,
    get: (req, res) => handleEverything(code, req, res),
    post: (req, res) => handleEverything(code, req, res),
    put: (req, res) => handleEverything(code, req, res),
    delete: (req, res) => handleEverything(code, req, res),
  });
});
