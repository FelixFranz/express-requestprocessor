const RequestProcessor = require(`${__dirname}/../../src/RequestProcessor`);

/**
 * Sums up two values provided by path parameters
 * @group Sum
 * @route GET /sum/{a}/{b}
 * @param {Integer} a.path.required Variable a
 * @param {Integer} b.path.required Variable b
 * @return {number} 200 - Result of the sum
 */

/**
 * Validates query parameters
 * @param {Object} req Request Object
 */
function validateGet(req) {
  if (Number.isNaN(parseInt(req.path.a)) ||
    Number.isNaN(parseInt(req.path.b))) {
    throw new Error('Only numbers are allowed as parameter');
  }
}

/**
 * Sums up two values provided by path parameters
 * @example GET http://127.0.0.1:8080/sum?a=2&b=4
 * @param {Object} req
 * @param {Object} res
 * @return {string}
 */
function handleGet(req, res) {
  return parseInt(req.path.a) + parseInt(req.path.b);
};

RequestProcessor.process({
  path: '/sum/:a/:b',
  validate: validateGet,
  get: handleGet,
  role: 'services.sum',
  link: {
    name: 'Dispatcher',
    url: '/',
  },
});
