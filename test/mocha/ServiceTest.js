const assert = require('assert');
const request = require('request');
const RequestProcessor = require(`${__dirname}/../../src/index`);
const Config = require(`${__dirname}/../RequestProcessorConfig`);

describe('Service', () => {
  before(() => {
    RequestProcessor.configure(Config).start();
  });

  after(() => {
    RequestProcessor.stop();
  });

  describe('Status Codes', () => {
    /**
         * tests provided return status code
         * @param {Number} statusCode to test for
         * @param {function} done function from mocha
         */
    function testStatusCode(statusCode, done) {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/statuscode/${statusCode}`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(response.statusCode, statusCode,
            `Status code must be ${statusCode}`);
        done();
      });
    }

    it('should return status 200', (done) => {
      testStatusCode(200, done);
    });

    it('should return status 400', (done) => {
      testStatusCode(400, done);
    });

    it('should return status 500', (done) => {
      testStatusCode(500, done);
    });

    it('should return status 600', (done) => {
      testStatusCode(600, done);
    });
  });

  describe('HTTP Methods', () => {
    it('should return status code 200 on GET', (done) => {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/statuscode/200`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(response.statusCode, 200, 'should return status code 200');
        done();
      });
    });

    it('should return status code 200 on POST', (done) => {
      request({
        method: 'POST',
        uri: `http://127.0.0.1:${Config.port}/statuscode/200`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(response.statusCode, 200, 'should return status code 200');
        done();
      });
    });

    it('should return status code 200 on PUT', (done) => {
      request({
        method: 'PUT',
        uri: `http://127.0.0.1:${Config.port}/statuscode/200`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(response.statusCode, 200, 'should return status code 200');
        done();
      });
    });

    it('should return status code 200 on DELETE', (done) => {
      request({
        method: 'DELETE',
        uri: `http://127.0.0.1:${Config.port}/statuscode/200`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(response.statusCode, 200, 'should return status code 200');
        done();
      });
    });

    it('should return status 405 if method is not implemented', (done) => {
      request({
        method: 'POST',
        uri: `http://127.0.0.1:${Config.port}/sum`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(response.statusCode, 405, 'should return status code 405');
        done();
      });
    });
  });

  describe('Validator', () => {
    it('should fail validating the input', (done) => {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/sum?a=1&b=fail`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(response.statusCode, 400, 'should return status code 400');
        done();
      });
    });

    it('should succeed validating the input', (done) => {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/sum?a=1&b=1`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(response.statusCode, 200, 'should return status code 200');
        done();
      });
    });
  });

  describe('Link Header', () => {
    it('Dispatcher service should contain links to other services', (done) => {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        const link = response.headers.link;
        assert.ok(link.includes('</statuscode/200>; title="Statuscode 200"'));
        assert.ok(link.includes('</statuscode/400>; title="Statuscode 400"'));
        assert.ok(link.includes('</statuscode/500>; title="Statuscode 500"'));
        assert.ok(link.includes('</statuscode/600>; title="Statuscode 600"'));
        assert.ok(link.includes('</sum>; title="Sum"'));
        done();
      });
    });

    it('Sum service should have link to Dispatcher service', (done) => {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/sum`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        const link = response.headers.link;
        assert.ok(link.includes('</>; title="Dispatcher"'));
        done();
      });
    });
  });

  describe('Parameter', () => {
    it('should consume query parameters', (done) => {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/sum?a=6&b=2`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(parseInt(response.body), 8, 'Result must be 8');
        done();
      });
    });

    it('should consume path parameters', (done) => {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/sum/3/5`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(parseInt(response.body), 8, 'Result must be 8');
        done();
      });
    });
  });

  describe('Config', () => {
    it('should return not found page', (done) => {
      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/notfoundpage`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.equal(JSON.parse(response.body),
            'Sorry this page was not found!',
            'Result must be expected string!');
        done();
      });
    });
  });
});
