const assert = require('assert');
const {describe, it} = require('mocha');

describe('Logger', () => {
  it('should fail logging', (done) => {
    const Logger = require('../../src/Logger');
    try {
      Logger.log(Logger.levels.INFO, 'Fail test', 'Logger Test');
      assert.fail('It shoult thorow an exception');
    } catch (e) {
      done();
    }
  });

  it('should log a message', (done) => {
    const Logger = require('../../src/Logger');
    Logger.configure(Logger.levels.INFO, (out, message) => {
      assert.ok(message && typeof message === 'string' && message.length > 0);
      assert.equal(out.level, Logger.levels.INFO, 'Log has a wrong level');
      assert.equal(out.message, 'Fail test', 'Log has a wrong message');
      assert.equal(out.source, 'Logger Test', 'log has a wrong source');
      assert.equal(out.details, undefined, 'Log provides details');
      done();
    });
    Logger.log(Logger.levels.INFO, 'Fail test', 'Logger Test');
  });

  it('should log a message without having provided a logging level', (done) => {
    const Logger = require('../../src/Logger');
    Logger.configure(undefined, (out, message) => {
      assert.ok(message && typeof message === 'string' && message.length > 0);
      done();
    });
    Logger.log(Logger.levels.INFO, 'Fail test', 'Logger Test');
  });
});
