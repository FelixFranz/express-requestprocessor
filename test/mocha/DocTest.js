const assert = require('assert');
const request = require('request');
const RequestProcessor = require(`${__dirname}/../../src/index`);
const Config = require(`${__dirname}/../RequestProcessorConfig`);

describe('Docs', () => {
  before(() => {
    RequestProcessor.configure(Config).start();
  });

  after(() => {
    RequestProcessor.stop();
  });

  it('should reach the doc page', (done) => {
    request({
      method: 'GET',
      uri: `http://127.0.0.1:${Config.port}${Config.docs.route}`,
    }, function(error, response) {
      if (error) assert.fail('There must not be an error');
      assert.equal(response.statusCode, 200, 'should return status code 200');
      assert(response.body.includes('<title>Swagger UI</title>'),
          'should return a html page with title Swagger UI');
      done();
    });
  });

  it('should reach the doc json config', (done) => {
    request({
      method: 'GET',
      uri: `http://127.0.0.1:${Config.port}${Config.docs.route}.json`,
    }, function(error, response) {
      if (error) assert.fail('There must not be an error');
      assert.equal(response.statusCode, 200,
          'It should return status code 200');
      const body = JSON.parse(response.body);
      assert.equal(body.info.title, 'Request Processor Demo Services',
          'The title should be Request Processor Demo Services');
      done();
    });
  });
});
