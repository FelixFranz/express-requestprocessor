const assert = require('assert');
const {describe, it, afterEach} = require('mocha');
const request = require('request');

describe('RequestProcessor', () => {
  const RequestProcessor = require(`${__dirname}/../../src/index`);
  const Config = require(`${__dirname}/../RequestProcessorConfig`);

  describe('configure', () => {
    // is not possible anymore, because configure will be called before
    // it('should not allow start if configure is not called', (done) => {
    //   try {
    //     RequestProcessor.start();
    //     assert.fail('It should throw an exception');
    //   } catch (e) {
    //     done();
    //   }
    // });

    it('should throw an error on bad configuration', (done) => {
      try {
        const conf = {};
        RequestProcessor.configure(conf);
        assert.fail('It should throw an exception');
      } catch (e) {
        done();
      }
    });

    it('should add configuration', () => {
      RequestProcessor.configure(Config);
    });

    it('should log anything', (done) => {
      const conf = JSON.parse(JSON.stringify(Config));
      conf.log.output = (out, message)=>{
        assert.ok(typeof message === 'string' && message !== '');
        done();
      };
      RequestProcessor.configure(conf);
    });
  });

  describe('start', () => {
    afterEach(() => {
      RequestProcessor.stop();
    });

    it('should reach the dispatcher service', (done) => {
      RequestProcessor.configure(Config).start();

      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/`,
      }, function(error, response) {
        if (error) assert.fail('There must not be an error');
        assert.ok(response.headers['content-type'].includes('text/html'),
            'The content type need to be text/html');
        assert.equal(response.statusCode, 200, 'Status code must be 200');
        done();
      });
    });

    it('should log the start message', (done) => {
      const conf = JSON.parse(JSON.stringify(Config));
      conf.log.output = (out, message)=>{
        if (message.includes('[INFO]: RequestProcessor: Started listening on http://127.0.0.1:8080')) done();
      };
      RequestProcessor.configure(conf).start();
    });
  });

  describe('stop', (done) => {
    it('should log the stopping', (done) => {
      const conf = JSON.parse(JSON.stringify(Config));
      conf.log.output = (out, message)=>{
        if (message.includes('[INFO]: RequestProcessor: Stopped server')) {
          done();
        }
      };

      RequestProcessor.configure(conf).start().stop();
    });

    it('should stop the api', (done) => {
      RequestProcessor.configure(Config).start().stop();

      request({
        method: 'GET',
        uri: `http://127.0.0.1:${Config.port}/`,
      }, function(error, response) {
        assert.equal(error.code, 'ECONNREFUSED',
            'There must be a connection error');
        done();
      });
    }).timeout(3000);
  });

  describe('restart', () => {
    it('should execute all commands without any erros', () => {
      RequestProcessor.configure(Config).start().restart().stop();
    });
  });
});
