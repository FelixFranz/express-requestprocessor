const assert = require('assert');
const request = require('request');
const RequestProcessor = require(`${__dirname}/../../src/index`);
const Config = require(`${__dirname}/../RequestProcessorConfig`);
const validCredentials = 'Basic ' + Buffer.from('user:pass').toString('base64');

describe('Authorizer', () => {
  before(() => {
    RequestProcessor.configure(Config).start();
  });

  after(() => {
    RequestProcessor.stop();
  });

  it('should fail authorize', (done) => {
    request({
      method: 'GET',
      uri: `http://127.0.0.1:${Config.port}/user`,
      headers: {
        authorization: 'Basic abc',
      },
    }, function(error, response) {
      if (error) assert.fail('There must not be an error');
      assert.equal(response.statusCode, 401, 'should return status code 401');
      assert.equal(JSON.parse(response.body).error, 'Authorization failed',
          'It should return "Authorization failed"');
      done();
    });
  });

  it('should succeed authorize', (done) => {
    request({
      method: 'GET',
      uri: `http://127.0.0.1:${Config.port}/user`,
      headers: {
        authorization: validCredentials,
      },
    }, function(error, response) {
      if (error) assert.fail('There must not be an error');
      assert.equal(response.statusCode, 200, 'should return status code 200');
      done();
    });
  });

  it('should return user name', (done) => {
    request({
      method: 'GET',
      uri: `http://127.0.0.1:${Config.port}/user`,
      headers: {
        authorization: validCredentials,
      },
    }, function(error, response) {
      if (error) assert.fail('There must not be an error');
      assert.equal(JSON.parse(response.body), 'user',
          'It should return a user name called "user"');
      done();
    });
  });
});
