const RequestProcessor = require(`${__dirname}/../src/index`);
const Config = require(`${__dirname}/RequestProcessorConfig`);

RequestProcessor
    .configure(Config)
    .start();
